//
//  ViewController.swift
//  RealmTEST
//
//  Created by Влад Мади on 11.12.2019.
//  Copyright © 2019 Влад Мади. All rights reserved.
//

import UIKit
import RealmSwift

class ViewController: UIViewController {
    
    let storageManager = StorageManager()
    let dogs = realm.objects(Dog.self)
    let person = realm.objects(Person.self)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableOfPersons.delegate = self
        tableOfPersons.dataSource = self
        
        print(Realm.Configuration.defaultConfiguration.fileURL!)

        tableOfPersons.tableFooterView = UIView()
        tableOfPersons.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.tableOfPersons.reloadData()
    }
    
    
    @IBAction func addPersonAction(_ sender: Any) {
        
        self.present(createAlertForPerson(), animated: true)
        
    }
    
    @IBAction func addDogAction(_ sender: Any) {
        
        self.present (createAlertForDogs(), animated: true)
        
    }
    
    @IBOutlet weak var tableOfPersons: UITableView!
    
    func createAlertForDogs() -> UIAlertController {
        let alert = UIAlertController(title: "Введите данные о собаке", message: nil, preferredStyle: .alert)
        
        alert.addTextField(configurationHandler: nil)
        alert.textFields![0].placeholder = "Имя собаки"
        alert.addTextField(configurationHandler: nil)
        alert.textFields![1].placeholder = "Порда собаки"
        alert.addTextField(configurationHandler: nil)
        alert.textFields![2].placeholder = "Пол собаки"

        
        let okAction = UIAlertAction(title: "Ok", style: .default) { (action) in
            
            let currentDog = Dog()
            
            
            
            if self.dogs.count != 0 {
                currentDog.id = self.dogs.last!.id + 1
            } else {
                currentDog.id = 1
            }
            
            
            currentDog.name = alert.textFields![0].text!
            currentDog.breed = alert.textFields![1].text!
            currentDog.sex = alert.textFields![2].text!

            
            self.storageManager.addToDB(object: currentDog)
            
        }
        alert.addAction(okAction)
        
        return alert
    }
    
    func createAlertForPerson() -> UIAlertController {
        let alert = UIAlertController(title: "Введите данные о владельце", message: nil, preferredStyle: .alert)
        
        alert.addTextField(configurationHandler: nil)
        alert.textFields![0].placeholder = "Имя владельца собак"
        alert.addTextField(configurationHandler: nil)
        alert.textFields![1].placeholder = "Возраст владельца собак"
        
        let okAction = UIAlertAction(title: "Ok", style: .default) { (action) in
            
            let currentPerson = Person()
            
            
            
            if self.person.count != 0 {
                currentPerson.id = self.person.last!.id + 1
            } else {
                currentPerson.id = 1
            }
            
            
            currentPerson.name = alert.textFields![0].text!
            currentPerson.age = Int(alert.textFields![1].text!)!
            
            self.storageManager.addToDB(object: currentPerson)
            
            self.tableOfPersons.reloadData()
            
        }
        alert.addAction(okAction)
        
          let cancelAction = UIAlertAction(title: "Отменить", style: .destructive, handler: nil)
        
          alert.addAction(cancelAction)
        
        return alert
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showDogs"{
            let indexPath = tableOfPersons.indexPathForSelectedRow
            let destVC = segue.destination as! DogsTableViewController
            destVC.dogs = person[indexPath!.row].dogs
            destVC.navigationItem.title = person[indexPath!.row].name
            destVC.person = person[indexPath!.row]
        }
    }
    
}

extension ViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return person.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        // Создание клетки
        let cell = tableOfPersons.dequeueReusableCell(withIdentifier: "cell")
        
        // Работа с клеткой
        cell?.textLabel?.text = self.person[indexPath.row].name
        cell?.detailTextLabel?.text = String(self.person[indexPath.row].dogs.count)
        
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        performSegue(withIdentifier: "showDogs", sender: nil)
    }
    
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        
        //Кнопка, которая удаляет ячейку и данные из базы
        let deleteAction = UIContextualAction(style: .destructive, title: "Удалить") { (_, _, completion) in
            
            let dogsOfPersonForIndexPath = self.person[indexPath.row].dogs
            
            for dog in dogsOfPersonForIndexPath {
                self.storageManager.removeFromDB(object: dog)
            }
            
            self.storageManager.removeFromDB(object: self.person[indexPath.row])
            
            tableView.deleteRows(at: [indexPath], with: .fade)
            completion(true)
        }
        
        return UISwipeActionsConfiguration(actions: [deleteAction])
        
    }
    
    
}

