//
//  DetailsOfDog.swift
//  RealmTEST
//
//  Created by Влад Мади on 18.12.2019.
//  Copyright © 2019 Влад Мади. All rights reserved.
//

import UIKit

class DetailsOfDogViewController: UIViewController {

    var avatar = UIImage()
    var dog = Dog()
    
    @IBOutlet weak var setButton: UIButton!
    var birthDate = Date()
    
    var imageData = Data()
    
    @IBOutlet weak var avatarImageView: UIImageView!
    @IBOutlet weak var nameTF: UITextField!
    @IBOutlet weak var breedTF: UITextField!
    @IBOutlet weak var sexTF: UITextField!
    @IBOutlet weak var descriptionTextView: UITextView!
    @IBOutlet weak var ageTF: UITextField!
    
    @IBOutlet weak var saveOutlet: UIButton!
    
    @IBOutlet weak var addAvatarOutlet: UIButton!
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        nameTF.isEnabled = false
        breedTF.isEnabled = false
        sexTF.isEnabled = false
        descriptionTextView.isEditable = false
        ageTF.isEnabled = false
        addAvatarOutlet.isHidden = true
        setButton.isHidden = true
        
        self.nameTF.text = dog.name
        self.breedTF.text = dog.breed
        self.sexTF.text = dog.sex
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = .medium
        dateFormatter.locale = Locale(identifier: "ru_RU")
        
        let calendar = Calendar.current
        
        let now = Date()
        let ageComponents = calendar.dateComponents([.year, .month], from: dog.age, to: now)
        
        let yearsOld = ageComponents.year
        let monthOld = ageComponents.month
        
        self.ageTF.text = "\(yearsOld!) лет и \(monthOld!) месяцев"
        
        self.avatarImageView.image = UIImage(data: dog.avatar) ?? UIImage(named: "1")
        saveOutlet.isHidden = true
    }
    
    
    @IBAction func updateAction(_ sender: Any) {
        nameTF.isEnabled = true
        breedTF.isEnabled = true
        sexTF.isEnabled = true
        descriptionTextView.isEditable = true
        ageTF.isEnabled = true
        setButton.isHidden = false
        
        saveOutlet.isHidden = false
        addAvatarOutlet.isHidden = false
        avatarImageView.alpha = 0.5
        
    }
    
    @IBAction func saveAction(_ sender: Any) {
        nameTF.isEnabled = false
        breedTF.isEnabled = false
        sexTF.isEnabled = false
        descriptionTextView.isEditable = false
        ageTF.isEnabled = false
        setButton.isHidden = true
        
        try! realm.write {
            dog.name = nameTF.text ?? ""
            dog.breed = breedTF.text ?? ""
            dog.sex = sexTF.text ?? ""
            dog.age = birthDate
            dog.avatar = imageData
        }
        
        saveOutlet.isHidden = true
        addAvatarOutlet.isHidden = true
        avatarImageView.alpha = 1

        
        let dateFormatter = DateFormatter()
         dateFormatter.dateStyle = .medium
         dateFormatter.locale = Locale(identifier: "ru_RU")
         
         let calendar = Calendar.current
         
         let now = Date()
         let ageComponents = calendar.dateComponents([.year, .month], from: dog.age, to: now)
         
         let yearsOld = ageComponents.year
         let monthOld = ageComponents.month
         
         self.ageTF.text = "\(yearsOld!) лет и \(monthOld!) месяцев"
    }
    
    @IBAction func addAvatarTapped(_ sender: Any) {
        
        let alert = UIAlertController(title: "Прикрепить фото", message: "Откуда брать фотку?", preferredStyle: .actionSheet)
        
        let cameraAction = UIAlertAction(title: "Из камеры", style: .default) { (action) in
            let imagePicker = UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.sourceType = UIImagePickerController.SourceType.camera
            imagePicker.cameraCaptureMode = .photo
            
            self.present(imagePicker, animated: true, completion: nil)
        }
        
        let galleryAction = UIAlertAction(title: "Из галереи", style: .default) { (action) in
            
            let imagePicker = UIImagePickerController()
            imagePicker.delegate = self
            self.present(imagePicker, animated: true, completion: nil)
            
        }
        
        let cancelAction = UIAlertAction(title: "Отмена", style: .cancel, handler: nil)
        
        alert.addAction(cameraAction)
        alert.addAction(galleryAction)
        alert.addAction(cancelAction)
        
        self.present(alert, animated: true, completion: nil)
    }
    
    
    @IBAction func setAgeAction(_ sender: Any) {
        
        let alert = UIAlertController(title: "Введите дату рождения", message: nil, preferredStyle: .alert)
        let datePicker = UIDatePicker()
        datePicker.center.x = alert.view.center.x
        datePicker.center.y = alert.view.center.y
        let pickerFrame = CGRect(x: 5, y: 0, width: datePicker.frame.width - 60, height: 350)
        datePicker.frame = pickerFrame
        
        let now = Date()
        datePicker.maximumDate = now
        
        datePicker.locale = Locale(identifier: "ru_RU")
        datePicker.datePickerMode = .date
        
        alert.view.addSubview(datePicker)
        alert.view.addConstraint(NSLayoutConstraint(item: alert.view!, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: datePicker.frame.height + 50))
        
        let okAction = UIAlertAction(title: "OK", style: .default) { (action) in
            self.birthDate = datePicker.date
            
            let dateFormatter = DateFormatter()
            dateFormatter.locale = Locale(identifier: "ru_RU")
            dateFormatter.dateStyle = .short
            
            let calendar = Calendar.current
            let ageComponents = calendar.dateComponents([.year, .month, .day], from: self.dog.age)
            
            
            self.ageTF.text = "\(ageComponents.day!).\(ageComponents.month!).\(ageComponents.year!)"
            
            
        }
        
        alert.addAction(okAction)
        
        
        
        self.present(alert, animated: true, completion: nil)
    }
    
    
//    func changeValue(toActive: Bool){
//        if toActive == true {
//            
//        } else {
//            
//        }
//    }
    
}

extension DetailsOfDogViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        let imageFromPC = info[UIImagePickerController.InfoKey.originalImage] as! UIImage
        self.imageData = imageFromPC.pngData()!
        
        avatar = UIImage(data: imageData)!
        avatarImageView.image = UIImage(data: imageData)
        self.dismiss(animated: true, completion: nil)
        
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        self.dismiss(animated: true, completion: nil)
    }
    
    
}
