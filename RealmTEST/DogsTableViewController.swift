//
//  DogsTableViewController.swift
//  RealmTEST
//
//  Created by Влад Мади on 16.12.2019.
//  Copyright © 2019 Влад Мади. All rights reserved.
//

import UIKit
import RealmSwift

class DogsTableViewController: UITableViewController {
    
    var storageManager = StorageManager()
    var person = Person()
    var dogs: List<Dog>!
    var dogsSummary: Results<Dog>!
    var avatarsArray = ["1"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        dogsSummary = realm.objects(Dog.self)
        tableViewSettings()
        
        if dogs.count == 0 {
            self.present(createAlertForDogs(), animated: true, completion: nil)
        }
        
        
        for i in person.dogs {
            print(i.name)
        }
        
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        tableView.reloadData()
    }
    
    func tableViewSettings() {
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(UINib(nibName: "DogCell", bundle: nil), forCellReuseIdentifier: DogCell.reuseID)
        tableView.backgroundColor = #colorLiteral(red: 0.2665522691, green: 0.497037836, blue: 0.9686274529, alpha: 1)
        tableView.separatorStyle = .none
//        tableView.allowsSelection = false
    }
    
    @IBAction func addDogAction(_ sender: Any) {
        self.present(createAlertForDogs(), animated: true)
    }
    
    func createAlertForDogs() -> UIAlertController {
        let alert = UIAlertController(title: "Введите данные о собаке", message: nil, preferredStyle: .alert)
        
        alert.addTextField(configurationHandler: nil)
        alert.textFields![0].placeholder = "Имя собаки"
        alert.addTextField(configurationHandler: nil)
        alert.textFields![1].placeholder = "Порда собаки"
        alert.addTextField(configurationHandler: nil)
        alert.textFields![2].placeholder = "Пол собаки"

        
        let okAction = UIAlertAction(title: "Ok", style: .default) { (action) in
            
            let currentDog = Dog()
            
            if self.dogsSummary.count != 0 {
                currentDog.id = self.dogsSummary.last!.id + 1
            } else {
                currentDog.id = 1
            }

            currentDog.name = alert.textFields![0].text!
            currentDog.breed = alert.textFields![1].text!
            currentDog.sex = alert.textFields![2].text!
            
            self.storageManager.addDogToPerson(dog: currentDog, person: self.person)
            self.tableView.reloadData()
            
        }
        
        alert.addAction(okAction)
        
        let cancelAction = UIAlertAction(title: "Отменить", style: .destructive, handler: nil)
        
        alert.addAction(cancelAction)
        
        return alert
    }
    
    func updateAlertForDogs(dog: Dog) -> UIAlertController {
        let alert = UIAlertController(title: "Введите новые данные о собаке", message: nil, preferredStyle: .alert)
        
        alert.addTextField(configurationHandler: nil)
        alert.textFields![0].text = dog.name
        alert.addTextField(configurationHandler: nil)
        alert.textFields![1].text = dog.breed
        alert.addTextField(configurationHandler: nil)
        alert.textFields![2].text = dog.sex

        
        let okAction = UIAlertAction(title: "Ok", style: .default) { (action) in
            
            try! realm.write {
                dog.name = alert.textFields![0].text!
                dog.breed = alert.textFields![1].text!
                dog.sex = alert.textFields![2].text!
            }
            
            self.tableView.reloadData()
        }
        
        alert.addAction(okAction)
        
        let cancelAction = UIAlertAction(title: "Отменить", style: .destructive, handler: nil)
        
        alert.addAction(cancelAction)
        
        return alert
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showDogDetail" {
            let indexPath = tableView.indexPathForSelectedRow
    
            let destVC = segue.destination as! DetailsOfDogViewController

            destVC.dog = person.dogs[indexPath?.row ?? 0]
            
        }
    }

    
    // MARK: - Table view data source
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return dogs.count 
    }
    
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: DogCell.reuseID, for: indexPath) as! DogCell
        cell.nameLabel.text = dogs[indexPath.row].name
        cell.ageLabel.text = "\(dogs[indexPath.row].age) лет"
        cell.breedLabel.text = dogs[indexPath.row].breed
        cell.sexLabel.text = dogs[indexPath.row].sex
        cell.avatar.image = UIImage(data: dogs[indexPath.row].avatar) ?? UIImage(named: "1")
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 145
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        performSegue(withIdentifier: "showDogDetail", sender: nil)
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    override func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        
        //Кнопка, которая удаляет ячейку и данные из базы
        let deleteAction = UIContextualAction(style: .destructive, title: "Удалить") { (_, _, completion) in
            
            self.storageManager.removeFromDB(object: self.dogs[indexPath.row])
            tableView.deleteRows(at: [indexPath], with: .fade)
            
            completion(true)
        }
        
        return UISwipeActionsConfiguration(actions: [deleteAction])
        
    }
    
    
    override func tableView(_ tableView: UITableView, leadingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        let updateAction = UIContextualAction(style: .normal, title: "Изменить") { (_, _, completion) in
            
            self.present(self.updateAlertForDogs(dog: self.dogs[indexPath.row]), animated: true, completion: nil)
            
            completion(true)
        }
        
        updateAction.backgroundColor = #colorLiteral(red: 0.1702981049, green: 0.6273246837, blue: 0.1523400944, alpha: 1)
        return UISwipeActionsConfiguration(actions: [updateAction])
    }
    
    

}
