//
//  Person.swift
//  RealmTEST
//
//  Created by Влад Мади on 11.12.2019.
//  Copyright © 2019 Влад Мади. All rights reserved.
//

import Foundation
import RealmSwift

class Person: Object {
    @objc dynamic var id = 0
    @objc dynamic var name = ""
    @objc dynamic var age = 0
    var dogs = List<Dog>()
    
    override class func primaryKey() -> String? {
        return "id"
    }
}


