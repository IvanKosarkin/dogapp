//
//  Dog.swift
//  RealmTEST
//
//  Created by Влад Мади on 11.12.2019.
//  Copyright © 2019 Влад Мади. All rights reserved.
//

enum Sex {
    case male
    case female
}

import Foundation
import RealmSwift

class Dog: Object {
    
    @objc dynamic var id: Int = 0
    @objc dynamic var name: String = ""
    @objc dynamic var breed: String = ""
    @objc dynamic var sex: String = ""
    @objc dynamic var age = Date()
    @objc dynamic var avatar = Data()
    @objc dynamic var dogDescription = ""
    
    override class func primaryKey() -> String? {
        return "id"
    }
    
}
