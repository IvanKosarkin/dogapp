//
//  StorageManager.swift
//  RealmTEST
//
//  Created by Влад Мади on 11.12.2019.
//  Copyright © 2019 Влад Мади. All rights reserved.
//

import Foundation
import RealmSwift

let realm = try! Realm()

class StorageManager {
    
    func addToDB(object: Object) {
        try! realm.write {
            realm.add(object)
        }
    }

    func removeFromDB(object: Object)  {
        try! realm.write {
            realm.delete(object)
        }
    }
    
    func addDogToPerson(dog: Dog, person: Person) {
        try! realm.write {
            person.dogs.append(dog)
        }
    }

    
}
