//
//  DogCell.swift
//  RealmTEST
//
//  Created by Влад Мади on 16.12.2019.
//  Copyright © 2019 Влад Мади. All rights reserved.
//

import UIKit

class DogCell: UITableViewCell {
    
    static let reuseID = "DogCell"
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var breedLabel: UILabel!
    @IBOutlet weak var ageLabel: UILabel!
    @IBOutlet weak var sexLabel: UILabel!
    @IBOutlet weak var cardView: UIView!
    @IBOutlet weak var avatar: UIImageView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        cardView.layer.cornerRadius = 10
        cardView.clipsToBounds = true
        self.backgroundColor = .clear
        avatar.clipsToBounds = true
        avatar.layer.cornerRadius = avatar.frame.height / 2
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }

}
